# Secret Not Tellable

[> Accéder au jeu <](https://degrangem.forge.apps.education.fr/SecretNotTellable/)

Secret Not Tellable est un jeu sérieux dans lequel les élèves doivent trouver un mot de passe respectant certaines contraintes … liées au programme de SNT !

Le jeu comporte 42 niveaux. Un très bon élève devrait pouvoir terminer le jeu en une heure. En moyenne les élèves atteignent le niveau 30 en une séance.

![image](/uploads/0c306e97808538f34dc26349e34e7760/image.png)

Ce jeu est une adptation du jeu [The Password Game](https://neal.fun/password-game/).

Vous pourrez trouver un article présentant le jeu ici : https://scape.enepe.fr/secret-not-tellable.html

## License
Ce jeu est placé sous licence MIT.

## Lien anti-triche
Un lien vers le code source du jeu est disponible en pied de page. Il est possible de masquer ce lien pour éviter une possible triche de la part des élèves qui pourraient trouver les réponses aux questions dans le code source du jeu. Il faut pour cela communiquer aux élèves [ce lien spécial](https://degrangem.forge.apps.education.fr/SecretNotTellable/#) (notez la présence du `#` à la fin de l'adresse URL).